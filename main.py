from multiprocessing.pool import ThreadPool

from src.config import other_domains, workers
from src.retriever import discover_articles, domains

if __name__ == '__main__':
    domains.extend(other_domains)
    pool = ThreadPool(workers)

    for domain in domains:
        # discover_articles(domain)
        pool.apply_async(discover_articles, (domain,))

    pool.close()
    pool.join()
