import newspaper

from core.queues import RabbitMQQueue
from core.logging.log import setup_logging
from src.config import rabbitmq_port, rabbitmq_user, rabbitmq_pass, rabbitmq_host, rabbitmq_topic


logger = setup_logging(__name__)

domains = [
    'https://techcrunch.com/',
    'https://www.wired.com/',
    'https://www.theverge.com/',
    'https://www.engadget.com/',
    'https://www.digitaltrends.com/',
    'https://www.techradar.com/',
    'https://www.cnet.com/',
    'https://www.techrepublic.com/',
    'https://www.zdnet.com/',
    'https://www.techspot.com/',
    'https://venturebeat.com/',
    'https://www.pcmag.com/'
]


def discover_articles(url):
    logger.info(f'Discovering articles for {url}')
    queue = RabbitMQQueue(user=rabbitmq_user,
                          password=rabbitmq_pass,
                          topic=rabbitmq_topic, host=rabbitmq_host,
                          port=rabbitmq_port)
    paper = newspaper.build(url, memoize_articles=False)
    for article in paper.articles:
        queue.send(article.url)
    logger.info(f'Finished discovering articles for {url}: {len(paper.articles)} articles found')
