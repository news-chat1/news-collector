import os

rabbitmq_host = os.getenv('RABBITMQ_HOST', 'localhost')
rabbitmq_port = int(os.getenv('RABBITMQ_PORT', '5672'))
rabbitmq_user = os.getenv('RABBITMQ_USER')
rabbitmq_pass = os.getenv('RABBITMQ_PASS')
rabbitmq_topic = os.getenv('RABBITMQ_TOPIC')


other_domains = os.getenv('OTHER_DOMAINS', '').split(',')
workers = int(os.getenv('WORKERS', '12'))