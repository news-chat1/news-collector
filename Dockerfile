FROM --platform=linux/amd64 python:3.8.18-alpine
LABEL authors="innocentkithinji"

WORKDIR /app

COPY . /app

RUN pip install -r requirements.txt

COPY root /var/spool/cron/crontabs/root

CMD crond -l 2 -f