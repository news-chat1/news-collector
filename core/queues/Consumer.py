from concurrent.futures import ThreadPoolExecutor
from typing import Callable

from core.logging import setup_logging
from ._interfaces import BaseConsumerInterface, BaseQueueInterface

logger = setup_logging(__name__)


class RabbitMQConsumer(BaseConsumerInterface):

    def __init__(self, queue: BaseQueueInterface, func: Callable[[tuple, BaseQueueInterface], None]):
        """Initializes the consumer

        Args:
            queue (BaseQueueInterface): The queue to consume from
        """
        self.queue: BaseQueueInterface = queue
        self.process_function = func

    def process_message(self, channel, method, properties, body):
        """Process a message.
        Args:
            channel: The channel the message was received on
            method: The method the message was received on
            properties: The properties of the message
            body: The body of the message
        """
        message = (channel, method, properties, body)
        self.process_function(message, self.queue)

    def consume(self):
        self.queue.connect()
        while True:
            try:
                self.queue.subscribe(callback=self.process_message)
            except Exception as e:
                logger.error(f'Error while consuming from queue', extra={'error': e})

    def stop_consuming(self):
        """Stops the consumer"""
        self.queue.disconnect()


class ThreadedRabbitMQConsumer:
    def __init__(self, queue_class, queue_params: tuple, process_function: Callable[[tuple, BaseQueueInterface], None], workers: int = 1):
        self.queue_params = queue_params
        self.queue_class = queue_class
        self.process_function = process_function
        self.workers: int = workers

    def run(self):
        with ThreadPoolExecutor(max_workers=self.workers) as executor:
            for _ in range(self.workers):
                queue_instance = self.queue_class(*self.queue_params)
                consumer = RabbitMQConsumer(queue_instance, self.process_function)
                executor.submit(consumer.consume)
