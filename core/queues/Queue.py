import json
from typing import Any, Callable, Optional

import pika

from ._interfaces import BaseQueueInterface


class RabbitMQQueue(BaseQueueInterface):
    def __init__(self, user: str, password: str, topic: str, host: str = 'localhost', port: int = 5672):
        """A RabbitMQ Service.

        Args:
            user (str): The username of the RabbitMQ server.
            password (str): The password of the RabbitMQ server.
            topic (str): The subscription id of the RabbitMQ server.
            host (str, optional): The host of the RabbitMQ server. Defaults to 'localhost'.
            port (int, optional): The port of the RabbitMQ server. Defaults to 5672.
        """

        self.topic: str = topic
        self._credentials: pika.PlainCredentials = pika.PlainCredentials(user, password)
        self._connection_params: pika.ConnectionParameters = pika.ConnectionParameters(host=host, port=port,
                                                                                       credentials=self._credentials,
                                                                                       heartbeat=6000)
        self.connection: pika.BlockingConnection = None
        self.channel: pika.adapters.blocking_connection.BlockingChannel = None
        self.connect()

    def connect(self):
        """Connect to the RabbitMQ server."""
        self.connection = pika.BlockingConnection(self._connection_params)
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=self.topic, durable=True, auto_delete=False, exclusive=False)
        self.channel.basic_qos(prefetch_count=1)

    def disconnect(self):
        """Disconnect from the RabbitMQ server."""
        if self.connection:
            self.connection.close()
            self.channel = None

    def send(self, message: Any, topic: Optional[str] = None):
        """Publish a message to a topic.

        Args:
            message (Any): The message to publish.
            topic (Optional[str], optional): The topic to publish to. Overwrites default topic.
        """
        if isinstance(message, dict):
            message = json.dumps(message)
        if topic is None:
            topic = self.topic

        if self.channel.is_closed:
            self.connect()

        self.channel.queue_declare(queue=topic, durable=True, auto_delete=False, exclusive=False)
        self.channel.basic_publish(exchange='',
                                   routing_key=topic,
                                   body=message)

    def subscribe(self, callback: Callable[[Any], None]):
        """Subscribe to a topic with a callback function.

        Args:
            callback (Callable[[Any], None]): The callback function to call when a message is received.
        """
        if self.channel.is_closed:
            self.connect()
        self.channel.queue_declare(queue=self.topic, durable=True, auto_delete=False, exclusive=False)
        self.channel.basic_consume(queue=self.topic, on_message_callback=callback, auto_ack=False)
        self.channel.start_consuming()

    def ack(self, message):
        """Acknowledge a message.
        Args:
            message: The message to acknowledge.

        """
        if self.channel.is_closed:
            self.connect()
        delivery_tag = message.delivery_tag
        self.channel.basic_ack(delivery_tag=delivery_tag)

    def get_message(self, timeout: Optional[int] = None) -> Any:
        """Poll a topic for a message with an optional timeout.

        Args:
            timeout (Optional[int], optional): The timeout in seconds. Defaults to None.
        """
        if self.channel.is_closed:
            self.connect()
        method_frame, header_frame, body = self.channel.basic_get()
        if method_frame:
            self.channel.basic_ack(method_frame.delivery_tag)
            return body
        return None
