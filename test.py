import json
from multiprocessing.pool import ThreadPool
from pprint import pprint

import requests
from bs4 import BeautifulSoup
from newspaper import Article

feed = 'https://abcnews.go.com/abcnews/technologyheadlines'

response = requests.get(feed)

webpage = response.content

soup = BeautifulSoup(webpage, features="xml")

items = soup.findAll('item')

links = []
articles = []

for item in items:
    link = item.link.text
    links.append(link)


def get_article_details(url):
    print(f'Getting article details for {url}')
    article = Article(url)
    article.download()
    article.parse()
    article.nlp()
    data = {
        'title': article.title,
        'text': article.text,
        'summary': article.summary,
        'keywords': article.keywords,
        'url': url,
        'top_image': article.top_image,
        'authors': article.authors,
        'publish_date': article.publish_date,
    }
    pprint(data)
    articles.append(data)
    return article


pool = ThreadPool(10)
for url in links:
    pool.apply_async(get_article_details, (url,))

pool.close()
pool.join()

with open('articles.json', 'w') as f:
    json.dump(articles, f, indent=4)
